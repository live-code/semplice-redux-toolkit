// core/store/http-status.store.ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../App';
import { addProduct } from '../../features/catalog/store/products.actions';

type Status =  'error' | 'success' | null;

interface HttpStatus {
  status: Status,
  actionType: string | null
}

export const httpStatusStore = createSlice({
  name: 'httpStatus',
  initialState: { status: null, actionType: null } as HttpStatus,
  reducers: {
    setErrorGlobal(state, action: PayloadAction<HttpStatus>) {
      state.status = action.payload.status;
      state.actionType = action.payload.actionType;
    }
  },
  extraReducers: {
    'products/add/rejected'  : (state, action) => {
      return {
        status: 'error' as Status,
        actionType: addProduct.rejected.type
      };
    },
    // TO FIX: typing error
    /*[addProduct.fulfilled]  : (state, action) => {
      console.log('errr')
      return true;
    }*/
  }
})

export const {
  setErrorGlobal
} = httpStatusStore.actions;

export const selectHttpStatus = (state: RootState) => state.httpStatus;


