import React  from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { decrement, increment, reset } from './store/counter/counter.actions';
import { setConfig } from './store/config/config.actions';
import { getCounter, getItemsPerPallet, getMaterial, getTotalPallets } from './store/counter/counter.selectors';

export const CounterPage: React.FC = () => {
  const counter = useSelector(getCounter);
  const totalPallets = useSelector(getTotalPallets)
  const itemsPerPallet = useSelector(getItemsPerPallet)
  const material = useSelector(getMaterial)
  const dispatch = useDispatch();

  console.log('render')
  return <div>
    <h3>Products: {counter}</h3>
    <h3>Pallets: {totalPallets} - ({itemsPerPallet} items per pallet)</h3>
    <h3>Material: {material}</h3>
    <button onClick={() => dispatch(decrement(5))}>-</button>
    <button onClick={() => dispatch(increment(10))}>+</button>
    <button onClick={() => dispatch(reset())}>reset</button>
    <hr/>
    <button onClick={() => dispatch(setConfig({ itemsPerPallet: 5}))}>set 5 items per pallet</button>
    <button onClick={() => dispatch(setConfig({ itemsPerPallet: 12}))}>set 12 items per pallet</button>
    <button onClick={() => dispatch(setConfig({ material: 'plastic'}))}>Material (Plastic)</button>
    <button onClick={() => dispatch(setConfig({ material: 'wood'}))}>Material (wood)</button>

  </div>
};
