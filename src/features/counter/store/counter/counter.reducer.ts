import { decrement, increment, reset } from './counter.actions';
import { createReducer } from '@reduxjs/toolkit';
/*

export function counterReducer(state = 0, action: any) {
  console.log(action)
  switch (action.type) {
    case increment.type:
      return state + action.payload;
    case decrement.type:
      return state - action.payload;
    default:
      return state;
  }
}
*/

export const counterReducer = createReducer(0, {
  [increment.type]: (state, action) => state + action.payload,
  [decrement.type]: (state, action) => state - action.payload,
  [reset.type]: () => 0,
})
