import { createReducer, PayloadAction } from '@reduxjs/toolkit';
import { Config } from '../../model/config';
import { setConfig } from './config.actions';

const initialState = { itemsPerPallet: 12, material: 'wood'};

export const configReducer = createReducer<Config>(initialState, {
  [setConfig.type]: (state: Config, action: PayloadAction<Partial<Config>>) => {
    return {
      ...state,
      ...action.payload
    }
  }
})
