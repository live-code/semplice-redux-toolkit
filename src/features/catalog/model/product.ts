export interface Product {
  id: number;
  price: number;
  title: string;
  visibility: boolean;
  categoryID: number
}
