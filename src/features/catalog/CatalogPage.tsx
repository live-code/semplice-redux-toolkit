import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addProduct, deleteProduct, getProducts, toggleProduct } from './store/products.actions';
import {
  getCategories,
  selectProducts,
  selectProductsByCat,
  selectProductsError,
  selectTotalProducts
} from './store/products.selectors';
import { ProductsList } from './components/ProductsList';
import { ProductsForm } from './components/ProductsForm';
import { selectHttpStatus } from '../../core/store/http-status.store';
import { Product } from './model/product';
import { AppDispatch } from '../../App';

export const CatalogPage: React.FC = () => {
  const categories = useSelector(getCategories)
  const [activeCatID, setActiveCatID] = useState<number>(categories[0].id)
  const products = useSelector(selectProductsByCat(activeCatID))
  const error = useSelector(selectProductsError)
  const httpStatus = useSelector(selectHttpStatus);

  const total = useSelector(selectTotalProducts);
  const dispatch = useDispatch() as AppDispatch;

  useEffect(() => {
    dispatch(getProducts())
  }, [dispatch])

  function addHandler(product: Partial<Product>) {
      dispatch(addProduct(product))
        .then(res => console.log('QUIII', res))
  }

  return <div>

    {/*{ error &&  <div className="alert alert-danger">server side error</div>}*/}
    {JSON.stringify(httpStatus)}
    {
      (httpStatus.status === 'error' && httpStatus.actionType === 'getProducts')
      && <div className="alert alert-danger">Get Products Error</div>
    }
    <ProductsForm
      categories={categories}
      onSubmit={addHandler}
    />

    <hr/>
    Filtri di ricerca
    {
      categories.map(c => {
        return <button
          key={c.id}
          onClick={() => setActiveCatID(c.id)}>{c.name}</button>
      })

    }
    <ProductsList
       products={products}
       total={total}
       onToggle={(product) => dispatch(toggleProduct(product))}
       onDelete={id => dispatch(deleteProduct(id))}
     />

  </div>
};
