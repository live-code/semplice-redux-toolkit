import { createSlice } from '@reduxjs/toolkit';

export const categoriesStore = createSlice({
  name: 'categories',
  initialState: [
    { id: -1, name: 'All'},
    { id: 1, name: 'Latticini'},
    { id: 2, name: 'Frutta'}
  ],
  reducers: {

  }
})
