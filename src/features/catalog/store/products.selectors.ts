import { RootState } from '../../../App';

export const selectProducts = (state: RootState) => state.catalog.list.entities;
export const selectProductsError = (state: RootState) => state.catalog.list.error;
export const selectTotalProducts = (state: RootState) => state.catalog.list.entities.reduce((acc, product) => {
  return acc + product.price
}, 0);

export const selectProductsByCat = (id: number) => (state: RootState) => {
  if (id === -1) {return state.catalog.list.entities};
  return state.catalog.list.entities.filter(p => p.categoryID === id )
}

export const getCategories = (state: RootState) => state.catalog.categories;
