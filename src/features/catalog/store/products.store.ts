import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Product } from '../model/product';

export const productStore = createSlice({
  name: 'products',
  initialState: {
    entities: [] as Product[],
    error: false
  },
  reducers: {
    getProductsSuccess(state, action: PayloadAction<Product[]>) {
      return {
        entities: action.payload,
        error: false,
      };
    },
    setError(state, action: PayloadAction<void>) {
      state.error = true;
    },
    addProductsSuccess(state, action: PayloadAction<Product>) {
      state.error = false;
      state.entities.push(action.payload);
    },
    deleteProductsSuccess(state, action: PayloadAction<number>) {
      state.error = false;
      const index = state.entities.findIndex(p => p.id === action.payload);
      state.entities.splice(index, 1)
    },
    toggleProductVisibility(state, action: PayloadAction<Product>) {
      const product = state.entities.find(p => p.id === action.payload.id);
      if (product) {
        product.visibility = action.payload.visibility;
      }
    }
  }
})

export const {
  addProductsSuccess,
  deleteProductsSuccess,
  getProductsSuccess,
  toggleProductVisibility,
  setError
} = productStore.actions
