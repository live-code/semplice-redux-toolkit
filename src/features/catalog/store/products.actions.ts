import { AppThunk } from '../../../App';
import { Product } from '../model/product';
import { getProductsSuccess, deleteProductsSuccess, addProductsSuccess, toggleProductVisibility, setError } from './products.store';
import Axios from 'axios';
import { setErrorGlobal } from '../../../core/store/http-status.store';
import { createAsyncThunk } from '@reduxjs/toolkit';

export const getProducts = (): AppThunk => async (dispatch) => {
  try {
    const response = await Axios.get<Product[]>('http://localhost:3001/products');
    dispatch(getProductsSuccess(response.data))
  } catch(e) {
    dispatch(setErrorGlobal({ status: 'error', actionType: 'getProducts'}))
  }
}

export const deleteProduct = (id: number): AppThunk => async (dispatch) => {
  try {
    const response = Axios.delete('http://localhost:3001/products/' + id)
      .then(res => {
        dispatch(deleteProductsSuccess(res.data))
      })
  } catch(e) {
    dispatch(setError())

  }
}
/*
export const addProduct = (
  product: Partial<Product>
): AppThunk => async (dispatch) => {
  try {
    const response = await Axios.post<Product>('http://localhost:3001/products/', {
      ...product,
      visibility: false
    });
    dispatch(addProductsSuccess(response.data))
  } catch(e) {
    dispatch(setError())

  }
}*/

export const addProduct = createAsyncThunk<
  Product,
  Partial<Product>
>(
  'products/add',
  async (
    payload,
    {  dispatch, rejectWithValue }
  )  => {
    try {
      const newProduct = await Axios.post<Product>(
        'http://localhost:3001/products',
        { ...payload, visibility: false }
      );
      dispatch(addProductsSuccess(newProduct.data))
      return newProduct.data;
    } catch (err) {
     // dispatch(setErrorGlobal({ status: 'error', actionType: 'addProduct' }))
      return rejectWithValue('errore!')
    }

  }
)




export const toggleProduct = (product: Product): AppThunk => async (dispatch) => {
  try {
    const response = await Axios.patch<Product>('http://localhost:3001/products/' + product.id, {
      // ...product,
      visibility: !product.visibility
    })
    dispatch(toggleProductVisibility(response.data))
  } catch(err) {
    dispatch(setError())

  }
}
