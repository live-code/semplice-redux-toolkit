import { combineReducers } from "@reduxjs/toolkit";
import { productStore } from './products.store';
import { categoriesStore } from './categoriesStore';

export const catalogReducers = combineReducers({
  list: productStore.reducer,
  categories: categoriesStore.reducer
})
