import React, { useState } from 'react';
import classNames from 'classnames';
import { Product } from '../model/product';
import { Category } from '../model/category';

export interface ProductsFormProps {
  categories: Category[];
  onSubmit: (productTitle: Partial<Product>) => void
}

const initialState: Partial<Product> = { title: '', price: 0, categoryID: -1};

export const ProductsForm: React.FC<ProductsFormProps> = (props) => {
  const [data, setData] = useState<Partial<Product>>(initialState);
  const titleIsValid = data.title && data.title.length > 2;
  const priceIsValid = data.price && data.price > 0
  const valid = titleIsValid && priceIsValid;

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) {
    console.log(e.currentTarget.type)
    const t = e.currentTarget;
    const isNumber = ['number', 'select-one'].includes(t.type)
    setData({
      ...data,
      [t.name]: isNumber ? +t.value : t.value
    });
  }

  function onSubmitHandler(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    props.onSubmit(data);
    setData(initialState)
  }

  return <>
    {JSON.stringify(data)}
    <form onSubmit={onSubmitHandler}>
      <input
        type="text"
        className={
          classNames(
            'form-control',
            { 'is-valid': titleIsValid},
            {'is-invalid': !titleIsValid}
          )
        }
        value={data.title} onChange={onChangeHandler} name="title"
      />

      <input
        type="number"
        min={0}
        className={
          classNames(
            'form-control',
            { 'is-valid': priceIsValid},
            {'is-invalid': !priceIsValid}
          )
        }
        value={data.price} onChange={onChangeHandler} name="price"
      />

      <select onChange={onChangeHandler} name="categoryID" className="form-control">
        {
          props.categories.map(c => {
            return <option key={c.id} value={c.id}>{c.name}</option>
          })
        }

      </select>
        <button type="submit" disabled={!valid}>SAVE</button>
    </form>
  </>
};
