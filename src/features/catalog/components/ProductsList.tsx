import React  from 'react';
import { Product } from '../model/product';
import classNames from 'classnames';
import { deleteProduct, toggleProduct } from '../store/products.actions';

export interface ProductsListProps {
  products: Product[];
  total: number;
  onDelete: (id: number) => void,
  onToggle: (product: Product) => void,
}
export const ProductsList: React.FC<ProductsListProps> = (props) => {
  const { products, total, onToggle, onDelete } = props;

  function deleteHandler(id: number, e: React.MouseEvent<HTMLElement>) {
    e.stopPropagation()
    onDelete(id);
  }

  return <>
    {
      products.map((product: Product) => {
        return <li
          className="list-group-item"
          key={product.id}
        >
          <i
            style={{
              opacity: product.visibility ? 1 : 0.5
            }}
            className={
              classNames('fa', {'fa-eye': product.visibility, 'fa-eye-slash': !product.visibility})
            }
            onClick={() => onToggle(product)}
          />
          {product.title}
          <div className="pull-right">

            <i
              className="fa fa-trash"
              onClick={e => deleteHandler(product.id, e)}
            />
            {product.price}
          </div>
        </li>
      })
    }

    <div className="text-center">
      <div className="badge badge-dark">TOTAL: {total}</div>
    </div>
  </>
};
