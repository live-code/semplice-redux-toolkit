import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addNews, toggleNews, deleteNews } from './store/news.store';
import { getNewsList } from './store/news.selectors';

export const HomePage: React.FC = () => {
  const [publishState, setPublishState] = useState<string>('all');
  const newsList = useSelector(getNewsList(publishState));
  const dispath = useDispatch();

  function onChangeHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if(e.key === 'Enter') {
      dispath(addNews(e.currentTarget.value));
      e.currentTarget.value = '';
    }
  }

  function setVisibilityFilter(e: React.ChangeEvent<HTMLSelectElement>) {
    setPublishState(e.currentTarget.value)
  }

  return <div>
    <input type="text" onKeyPress={onChangeHandler}/>

    <hr/>

    <select onChange={setVisibilityFilter}>
      <option value="all">view all</option>
      <option value="published">published</option>
      <option value="unpublished">unpublished</option>
    </select>


    <div>
      {newsList.length ? null : <em>No Items</em>}
    </div>


    {
      newsList.map(news => {
        return (
          <li key={news.id} className="list-group-item">
            {news.title}

            <div
              className="badge badge-dark ml-3"
              onClick={() => dispath(toggleNews(news.id))}
            >
              {news.published ? 'PUBLISHED' : 'UNPUBLISHED'}
            </div>

            <i className="fa fa-trash" onClick={() => dispath(deleteNews(news.id))} />
          </li>
        )
      })
    }
  </div>
};
