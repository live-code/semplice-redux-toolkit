import { RootState } from '../../../App';

export const getNewsList = (publishState: string) => (state: RootState) => {
  switch(publishState) {
    case 'published': return state.news.filter(n => n.published);
    case 'unpublished': return state.news.filter(n => !n.published);
    default: return state.news;
  }
}
