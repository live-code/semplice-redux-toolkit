import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { HomePage } from './features/home/HomePage';
import { CounterPage } from './features/counter/CounterPage';
import { CatalogPage } from './features/catalog/CatalogPage';
import { UsersPage } from './features/users/UserPage';
import { Navbar } from './core/components/Navbar';
import { combineReducers, configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { counterReducers } from './features/counter/store';
import { newsStore } from './features/home/store/news.store';
import { catalogReducers } from './features/catalog/store';
import { httpStatusStore } from './core/store/http-status.store';

const rootReducer = combineReducers({
  counter: counterReducers,
  news: newsStore.reducer,
  catalog: catalogReducers,
  httpStatus: httpStatusStore.reducer
})

export const store = configureStore({
  reducer: rootReducer,
  devTools: process.env.NODE_ENV !== 'production'
})

export type RootState = ReturnType<typeof rootReducer>
export type AppDispatch = typeof store.dispatch

export type AppThunk = ThunkAction<void, RootState, null, Action<string>>;

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Navbar />
        <Switch>
          <Route path="/" exact>
            <HomePage />
          </Route>
          <Route path="/counter" exact>
            <CounterPage />
          </Route>
          <Route path="/catalog" exact>
            <CatalogPage/>
          </Route>
          <Route path="/users" exact>
            <UsersPage />
          </Route>
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
